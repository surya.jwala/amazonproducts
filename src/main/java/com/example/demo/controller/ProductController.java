package com.example.demo.controller;

import com.example.demo.controller.request.ProductRequestDto;
import com.example.demo.controller.response.ProductResponseDto;
import com.example.demo.model.Product;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @RequestMapping("/save")
    private ProductResponseDto addProductData(@RequestBody ProductRequestDto productRequestDto) {


        return productService.addProduct(productRequestDto);

    }

    @RequestMapping("/findall")
    private List<Product> getALl()
    {
        return productService.getAllProducts();
    }


}


